import os
import requests
import json
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/')
def index():
    return jsonify({"Message": "/search?q=<palavra-chave>"})

@app.route('/search')
def search():
    api_token= "032e15f707a00a9179e29c007306dc7e344c2b56"
    api_url_base="https://api.github.com"
    headers={"Authorization": "token {0}".format(api_token)}

    query = request.args.get('q', default='idna', type = str)

    api_url_search = '{0}/search/repositories?q={1}+language:{2}+language:{3}&sort=stars&order=desc'.format(api_url_base, query, 'java','python')
    response = requests.get(api_url_search, headers=headers)
    pretty_json = json.loads(response.content.decode('utf-8')) 
    res_dic = {}
    for item in pretty_json["items"]:
        res_dic[item["full_name"]] = item["html_url"]

    return (json.dumps(res_dic, indent=2))

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)
